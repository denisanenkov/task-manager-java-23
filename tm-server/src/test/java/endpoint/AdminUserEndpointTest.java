package endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.anenkov.tm.bootstrap.Bootstrap;
import ru.anenkov.tm.endpoint.AdminUserEndpoint;
import ru.anenkov.tm.endpoint.SessionEndpoint;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.user.AccessDeniedException;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.service.AuthService;
import ru.anenkov.tm.service.UserService;

public class AdminUserEndpointTest {

    Bootstrap bootstrap = new Bootstrap();
    AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(bootstrap);
    SessionEndpoint sessionEndpoint = new SessionEndpoint(bootstrap);
    UserEndpoint userEndpoint = new UserEndpoint(bootstrap);

    @Before
    public void before() {
        bootstrap.initUsers();
    }

    @Test (expected = AccessDeniedException.class)
    public void lockUserTest() {
        final Session session = sessionEndpoint.openSession("1","1");
        User user = userEndpoint.findByLoginUser(session, "2");
        Assert.assertFalse(user.isLocked());
        adminUserEndpoint.lockUserByLogin(session, user.getLogin());
        Assert.assertTrue(user.isLocked());
        new AuthService(new UserService(new UserRepository())).login("2", "2");
    }

    @Test
    public void unlockUserTest() {
        final Session session = sessionEndpoint.openSession("admin","admin");
        User user = userEndpoint.findByLoginUser(session, "1");
        Assert.assertFalse(user.isLocked());
        adminUserEndpoint.lockUserByLogin(session, user.getLogin());
        Assert.assertTrue(user.isLocked());
        adminUserEndpoint.unlockUserByLogin(session, user.getLogin());
        Assert.assertFalse(user.isLocked());
    }

    @Test
    public void removeUser() {
        final Session session = sessionEndpoint.openSession("admin","admin");
        User user = userEndpoint.findByLoginUser(session, "1");
        Assert.assertEquals(userEndpoint.findAllUser(session).size(), 4);
        adminUserEndpoint.removeUser(session, user);
        Assert.assertEquals(userEndpoint.findAllUser(session).size(), 3);
        user = userEndpoint.findByLoginUser(session, "2");
        adminUserEndpoint.removeByIdUser(session, user.getId());
        Assert.assertEquals(userEndpoint.findAllUser(session).size(), 2);
        User user2 = userEndpoint.findByLoginUser(session, "test");
        adminUserEndpoint.removeByIdUser(session, user2.getId());
        Assert.assertEquals(userEndpoint.findAllUser(session).size(), 1);
    }



}
