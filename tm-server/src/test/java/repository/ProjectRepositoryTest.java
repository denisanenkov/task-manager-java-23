package repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepositoryTest {

    ProjectRepository projectRepository = new ProjectRepository();
    UserRepositoryTest userRepositoryTest = new UserRepositoryTest();
    Project project = new Project();

    @Before
    public void beforeTest() {
        project.setUserId(userRepositoryTest.user.getId());
        project.setName("First Project");
        project.setDescription("Description of Project");
    }

    @Test
    public void testCreate() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(userRepositoryTest.user.getId(), project);
        Assert.assertFalse(projectRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
        List<Project> projects = new ArrayList<>();
        projects.add(project);
        Assert.assertEquals(projectRepository.findAll(userRepositoryTest.user.getId()), projects);
        Assert.assertEquals(project.getName(), "First Project");
        Assert.assertEquals(project.getDescription(), "Description of Project");
    }

    @Test
    public void testFind() {
        Project project1 = new Project();
        Project project2 = new Project();
        Project project3 = new Project();
        projectRepository.add(userRepositoryTest.user.getId(), project);
        project1 = projectRepository.findOneById(userRepositoryTest.user.getId(), project.getId());
        Assert.assertNotNull(project1);
        project2 = projectRepository.findOneByIndex(userRepositoryTest.user.getId(), 0);
        Assert.assertNotNull(project2);
        project3 = projectRepository.findOneByName(userRepositoryTest.user.getId(), "First Project");
        Assert.assertNotNull(project3);
    }

    @Test
    public void testRemove() {
        projectRepository.add(userRepositoryTest.user.getId(), project);
        Project project1 = projectRepository.removeOneById(userRepositoryTest.user.getId(), project.getId());
        Assert.assertNotNull(project1);
        Assert.assertTrue(projectRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
        projectRepository.add(userRepositoryTest.user.getId(), project);
        Project project2 = projectRepository.removeOneByIndex(userRepositoryTest.user.getId(), 0);
        Assert.assertNotNull(project2);
        Assert.assertTrue(projectRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
        projectRepository.add(userRepositoryTest.user.getId(), project);
        Project project3 = projectRepository.removeOneByName(userRepositoryTest.user.getId(), "First Project");
        Assert.assertNotNull(project3);
        Assert.assertTrue(projectRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
    }

    @Test
    public void testClear() {
        projectRepository.add(userRepositoryTest.user.getId(), project);
        Assert.assertFalse(projectRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
    }

}
