package repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public class TaskRepositoryTest {
    TaskRepository taskRepository = new TaskRepository();
    UserRepositoryTest userRepositoryTest = new UserRepositoryTest();
    Task task = new Task();

    @Before
    public void beforeTest() {
        task.setUserId(userRepositoryTest.user.getId());
        task.setName("First Task");
        task.setDescription("Description of Task");
    }

    @Test
    public void testCreate() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(userRepositoryTest.user.getId(), task);
        Assert.assertFalse(taskRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
        List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        Assert.assertEquals(taskRepository.findAll(userRepositoryTest.user.getId()), tasks);
        Assert.assertEquals(task.getName(), "First Task");
        Assert.assertEquals(task.getDescription(), "Description of Task");
    }

    @Test
    public void testFind() {
        Task task1;
        Task task2;
        Task task3;
        taskRepository.add(userRepositoryTest.user.getId(), task);
        task1 = taskRepository.findOneById(userRepositoryTest.user.getId(), task.getId());
        Assert.assertNotNull(task1);
        task2 = taskRepository.findOneByIndex(userRepositoryTest.user.getId(), 0);
        Assert.assertNotNull(task2);
        task3 = taskRepository.findOneByName(userRepositoryTest.user.getId(), "First Task");
        Assert.assertNotNull(task3);
    }

    @Test
    public void testRemove() {
        taskRepository.add(userRepositoryTest.user.getId(), task);
        Task task1 = taskRepository.removeOneById(userRepositoryTest.user.getId(), task.getId());
        Assert.assertNotNull(task1);
        Assert.assertTrue(taskRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
        taskRepository.add(userRepositoryTest.user.getId(), task);
        Task task2 = taskRepository.removeOneByIndex(userRepositoryTest.user.getId(), 0);
        Assert.assertNotNull(task2);
        Assert.assertTrue(taskRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
        taskRepository.add(userRepositoryTest.user.getId(), task);
        Task task3 = taskRepository.removeOneByName(userRepositoryTest.user.getId(), "First Task");
        Assert.assertNotNull(task3);
        Assert.assertTrue(taskRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
    }

    @Test
    public void testClear() {
        taskRepository.add(userRepositoryTest.user.getId(), task);
        Assert.assertFalse(taskRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
        taskRepository.clear();
        Assert.assertTrue(taskRepository.findAll(userRepositoryTest.user.getId()).isEmpty());
    }

}
