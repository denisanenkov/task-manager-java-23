package ru.anenkov.tm.api.service;

public interface IPropertyService {

    void init();

    String getServerHost();

    Integer getServerPort();

    String getSessionSalt();

    Integer getSessionCycle();

}
