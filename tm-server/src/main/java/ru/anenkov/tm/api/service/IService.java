package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.AbstractEntity;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

public interface IService<T extends AbstractEntity> {

    void load(@Nullable List<T> t);

    void load(@Nullable T... t);

    @Nullable
    List<T> getList();

    @NotNull
    void merge(@Nullable T element);

    void merge(@Nullable Collection<T> elements);

    void merge(@Nullable T... elements);

    void clearAll();

}
