package ru.anenkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAdminUserEndpoint {

    @WebMethod
    void lockUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "login", partName = "login") final String login
    );

    @WebMethod
    void unlockUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "login", partName = "login") final String login
    );

    @WebMethod
    void deleteUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "login", partName = "login") final String login
    );

    @WebMethod
    @NotNull User removeUser(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "user", partName = "user") final User user
    );

    @WebMethod
    @NotNull User removeByIdUser(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    @NotNull User removeByLoginUser(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "login", partName = "login") final String login
    );

    @WebMethod
    @NotNull User removeByEmailUser(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "email", partName = "email") final String email
    );

}
