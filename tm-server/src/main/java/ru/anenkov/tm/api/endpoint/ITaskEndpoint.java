package ru.anenkov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    @SneakyThrows
    void create(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "name", partName = "name") final String name,
            @NotNull @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    @SneakyThrows
    void add(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "session", partName = "session") final Task task
    );

    @WebMethod
    @SneakyThrows
    void remove(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "task", partName = "task") final Task task
    );

    @WebMethod
    @SneakyThrows
    List<Task> findAll(
            @NotNull @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    @SneakyThrows
    void clear(
            @NotNull @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    @SneakyThrows
    Task findOneByIndex(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "index", partName = "index") final Integer index
    );

    @WebMethod
    @SneakyThrows
    Task findOneByName(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "name", partName = "name") final String name
    );

    @WebMethod
    @SneakyThrows
    Task findOneById(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    @SneakyThrows
    @NotNull Task removeOneByIndex(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "index", partName = "index") final Integer index
    );

    @WebMethod
    @SneakyThrows
    @NotNull Task removeOneByName(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "name", partName = "name") @Nullable String name
    );

    @WebMethod
    @SneakyThrows
    @NotNull Task removeOneById(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    @SneakyThrows
    @NotNull Task updateTaskById(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "id", partName = "id") final String id,
            @NotNull @WebParam(name = "name", partName = "name") final String name,
            @NotNull @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    @SneakyThrows
    @NotNull Task updateTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "index", partName = "index") final Integer index,
            @NotNull @WebParam(name = "name", partName = "name") final String name,
            @NotNull @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    void load(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "task", partName = "task") final List tasks
    );

    @WebMethod
    List<Task> getList(
            @NotNull @WebParam(name = "session", partName = "session") final Session session
    );

}
