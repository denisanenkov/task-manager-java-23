package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.endpoint.*;

public interface IEndpointService {

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

}
