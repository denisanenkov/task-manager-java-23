package ru.anenkov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-01-13T19:02:30.882+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.anenkov.ru/", name = "UserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface UserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/updateUserEmailRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/updateUserEmailResponse")
    @RequestWrapper(localName = "updateUserEmail", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.UpdateUserEmail")
    @ResponseWrapper(localName = "updateUserEmailResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.UpdateUserEmailResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.anenkov.tm.endpoint.User updateUserEmail(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session,
        @WebParam(name = "newEmail", targetNamespace = "")
        java.lang.String newEmail
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/findByIdUserRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/findByIdUserResponse")
    @RequestWrapper(localName = "findByIdUser", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.FindByIdUser")
    @ResponseWrapper(localName = "findByIdUserResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.FindByIdUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.anenkov.tm.endpoint.User findByIdUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/showUserProfileRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/showUserProfileResponse")
    @RequestWrapper(localName = "showUserProfile", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.ShowUserProfile")
    @ResponseWrapper(localName = "showUserProfileResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.ShowUserProfileResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.anenkov.tm.endpoint.User showUserProfile(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/updateUserMiddleNameRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/updateUserMiddleNameResponse")
    @RequestWrapper(localName = "updateUserMiddleName", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.UpdateUserMiddleName")
    @ResponseWrapper(localName = "updateUserMiddleNameResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.UpdateUserMiddleNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.anenkov.tm.endpoint.User updateUserMiddleName(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session,
        @WebParam(name = "newMiddleName", targetNamespace = "")
        java.lang.String newMiddleName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/updatePasswordUserRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/updatePasswordUserResponse")
    @RequestWrapper(localName = "updatePasswordUser", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.UpdatePasswordUser")
    @ResponseWrapper(localName = "updatePasswordUserResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.UpdatePasswordUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.anenkov.tm.endpoint.User updatePasswordUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session,
        @WebParam(name = "newPassword", targetNamespace = "")
        java.lang.String newPassword
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/findAllUserRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/findAllUserResponse")
    @RequestWrapper(localName = "findAllUser", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.FindAllUser")
    @ResponseWrapper(localName = "findAllUserResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.FindAllUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.anenkov.tm.endpoint.User> findAllUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/updateUserLastNameRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/updateUserLastNameResponse")
    @RequestWrapper(localName = "updateUserLastName", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.UpdateUserLastName")
    @ResponseWrapper(localName = "updateUserLastNameResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.UpdateUserLastNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.anenkov.tm.endpoint.User updateUserLastName(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session,
        @WebParam(name = "newLastName", targetNamespace = "")
        java.lang.String newLastName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/findByEmailUserRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/findByEmailUserResponse")
    @RequestWrapper(localName = "findByEmailUser", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.FindByEmailUser")
    @ResponseWrapper(localName = "findByEmailUserResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.FindByEmailUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.anenkov.tm.endpoint.User findByEmailUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/checkRoleUserRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/checkRoleUserResponse")
    @RequestWrapper(localName = "checkRoleUser", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.CheckRoleUser")
    @ResponseWrapper(localName = "checkRoleUserResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.CheckRoleUserResponse")
    public void checkRoleUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session,
        @WebParam(name = "roles", targetNamespace = "")
        java.util.List<ru.anenkov.tm.endpoint.Role> roles
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/updateUserFirstNameRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/updateUserFirstNameResponse")
    @RequestWrapper(localName = "updateUserFirstName", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.UpdateUserFirstName")
    @ResponseWrapper(localName = "updateUserFirstNameResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.UpdateUserFirstNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.anenkov.tm.endpoint.User updateUserFirstName(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session,
        @WebParam(name = "newFirstName", targetNamespace = "")
        java.lang.String newFirstName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/findByLoginUserRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/findByLoginUserResponse")
    @RequestWrapper(localName = "findByLoginUser", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.FindByLoginUser")
    @ResponseWrapper(localName = "findByLoginUserResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.FindByLoginUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.anenkov.tm.endpoint.User findByLoginUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/createUserRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/createUserResponse")
    @RequestWrapper(localName = "createUser", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.CreateUser")
    @ResponseWrapper(localName = "createUserResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.CreateUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.anenkov.tm.endpoint.User createUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/findByIdRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/findByIdResponse")
    @RequestWrapper(localName = "findById", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.FindById")
    @ResponseWrapper(localName = "findByIdResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.FindByIdResponse")
    public void findById(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session,
        @WebParam(name = "roles", targetNamespace = "")
        java.util.List<ru.anenkov.tm.endpoint.Role> roles
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/UserEndpoint/getListUserRequest", output = "http://endpoint.tm.anenkov.ru/UserEndpoint/getListUserResponse")
    @RequestWrapper(localName = "getListUser", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.GetListUser")
    @ResponseWrapper(localName = "getListUserResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.GetListUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.anenkov.tm.endpoint.User> getListUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.anenkov.tm.endpoint.Session session
    );
}
