package ru.anenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class UserUpdateFirstNameClientCommand extends AbstractCommandClient {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Update-first-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update first name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER NEW USER FIRST NAME: ");
        @NotNull final String newFirstName = TerminalUtil.nextLine();
        bootstrap.getUserEndpoint().updateUserFirstName(bootstrap.getSession(), newFirstName);
        System.out.println("[NAME UPDATE SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
