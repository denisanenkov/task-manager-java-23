package ru.anenkov.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;

public class DataXmlSaveCommand extends AbstractCommandClient {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Data-xml-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Data XML save";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML SAVE]");
        bootstrap.getAdminEndpoint().saveDataXml(bootstrap.getSession());
        System.out.println("[SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
