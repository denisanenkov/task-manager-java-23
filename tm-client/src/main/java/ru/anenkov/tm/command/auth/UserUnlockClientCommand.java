package ru.anenkov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.Locale;

public class UserUnlockClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Unlock-user";
    }

    @Override
    public @Nullable String description() {
        return "Unlock user (admin command)";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UNLOCK USER]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        bootstrap.getAdminUserEndpoint().unlockUserByLogin(bootstrap.getSession(), login);
        System.out.println("[UNLOCK USER WITH LOGIN \"" + login.toUpperCase(Locale.ROOT) + "\" SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
