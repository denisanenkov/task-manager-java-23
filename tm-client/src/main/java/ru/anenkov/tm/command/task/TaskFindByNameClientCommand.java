package ru.anenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.Task;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class TaskFindByNameClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Show-task-by-name";
    }

    @Override
    public @Nullable String description() {
        return "Show Task By Name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final Task task = bootstrap.getTaskEndpoint().findOneByName(bootstrap.getSession(), name);
        if (task == null) return;
        System.out.println("" +
                "NAME: " + task.getName() +
                ", \nDESCRIPTION: " + task.getDescription() +
                ", \nUSER ID: " + task.getUserId() +
                ", \nTASK ID: " + task.getId()
        );
        System.out.println("[SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
